package nl.sacation.projectscorekeeper;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import static android.R.attr.id;

public class MainActivity extends AppCompatActivity {

    int p1Score=0,p1YellowCards=0,p1RedCards=0;
    int p2Score=0,p2YellowCards=0,p2RedCards=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        UpdateCounters();
        findViewById(R.id.btn1Goal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p1Score++;
                UpdateCounters();
            }
        });

        findViewById(R.id.btn1YellowCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p1YellowCards++;
                UpdateCounters();
            }
        });

        findViewById(R.id.btn1RedCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p1RedCards++;
                UpdateCounters();
            }
        });

        findViewById(R.id.btn2Goal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p2Score++;
                UpdateCounters();
            }
        });

        findViewById(R.id.btn2YellowCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p2YellowCards++;
                UpdateCounters();
            }
        });

        findViewById(R.id.btn2RedCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                p2RedCards++;
                UpdateCounters();
            }
        });
        findViewById(R.id.btnReset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResetCounters();
            }
        });
    }
    public void ResetCounters(){
        p1Score=0;
        p1YellowCards=0;
        p1RedCards=0;
        p2Score=0;
        p2YellowCards=0;
        p2RedCards=0;
        UpdateCounters();

        Context context = getApplicationContext();
        Toast resetMess=
                Toast.makeText(context,"The scores have been resetted",Toast.LENGTH_SHORT);
        resetMess.show();

    }
    public void UpdateCounters(){
        TextView p1ScoreView=(TextView) findViewById(R.id.Player1Score);

        p1ScoreView.setText(String.format(getResources().getString(R.string.score), String.valueOf(p1Score)));
        TextView p1YellowView=(TextView) findViewById(R.id.Player1YellowCards);
        p1YellowView.setText(String.format(getResources().getString(R.string.yellowCards), String.valueOf(p1YellowCards)));
        TextView p1RedView=(TextView) findViewById(R.id.Player1RedCards);
        p1RedView.setText(String.format(getResources().getString(R.string.redCards), String.valueOf(p1RedCards)));

        TextView p2ScoreView=(TextView) findViewById(R.id.Player2Score);
        p2ScoreView.setText(String.format(getResources().getString(R.string.score), String.valueOf(p2Score)));
        TextView p2YellowView=(TextView) findViewById(R.id.Player2YellowCards);
        p2YellowView.setText(String.format(getResources().getString(R.string.yellowCards), String.valueOf(p2YellowCards)));
        TextView p2RedView=(TextView) findViewById(R.id.Player2RedCards);
        p2RedView.setText(String.format(getResources().getString(R.string.redCards), String.valueOf(p2RedCards)));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
